'use strict'

const isDev = require('./isdev')

exports.Distro = {
  url: 'https://cdn.vultanium.fr/launcher/distrib.json',
  path: 'distrib.json',
  devPath: 'dev_distrib.json'
}

exports.Config = {
  dataPath: '.vultanium',
  tempNativesFolder: 'VultaNatives',
  file: 'config.json'
}

exports.Sentry = {
  env: isDev ? 'dev' : 'prod',
  PublicKey: isDev ? '727cba358df84f4c82f3645b85542624' : '727cba358df84f4c82f3645b85542624',
  ProjectID: isDev ? '1383257' : '1383257'
}