'use strict'

import { init, showReportDialog } from '@sentry/electron'
import options from './options'
import LoggerUtil from './loggerutil'

const logger = new LoggerUtil('%c[Sentry]', 'color: #30283; font-weight: bold')

logger.log('Loading...')

init({
  dsn: `https://${options.Sentry.PublicKey}@sentry.io/${options.Sentry.ProjectID}`,
  release: require('../package.json').version,
  environment: options.Sentry.env,
  beforeSend(event, hint) {
    logger.log('Error!')
    showReportDialog()
    return event
  }
})

logger.log('Loaded!')