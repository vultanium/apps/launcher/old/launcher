'use strict'

// Work in progress
const { Client, register } = require('discord-rpc')
const logger = require('./loggerutil')('%c[DiscordWrapper]', 'color: #7289da; font-weight: bold')

let client
let activity

exports.initRPC = async (genSettings, servSettings, initialDetails = 'Waiting for Client..') => {
  register(genSettings.clientId)

  client = new Client({ transport: 'ipc' })

  activity = {
    state: 'Serveur: ' + servSettings.shortId,
    details: initialDetails,
    startTimestamp: new Date(),
    //endTimestamp: '',
    largeImageKey: servSettings.largeImageKey,
    largeImageText: servSettings.largeImageText,
    smallImageKey: genSettings.smallImageKey,
    smallImageText: genSettings.smallImageText,
    partyId: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    partySize: 2,
    partyMax: 5,
    spectateSecret: 'bbbbbbbbbbbbbbbbbbbbbbbbbbbbb',
    joinSecret: 'ccccccccccccccccccccccccccccccc',
    instance: true
  }

  try {
    logger.log('createLobby')
    await client.createLobby('Public', 5, ['a', '123'])
    logger.log('createLobby ok')
  } catch (err) {
    logger.log('createLobby Error!')
    logger.log(err)
  }

  client.on('ready', async () => {
    logger.log('Discord RPC Connected')
    await client.setActivity(activity)
    logger.log('Discord RPC Activity set')
  })

  try {
    await client.login({ clientId: genSettings.clientId })
    logger.log('Discord RPC Logged')
  } catch (error) {
    logger.log('Error!')
    if (error.message.includes('ENOENT')) logger.log('Unable to initialize Discord Rich Presence, no client detected.')
    else logger.log('Unable to initialize Discord Rich Presence: ' + error.message, error)
  }

  client.subscribe('GAME_JOIN', async data => {
    logger.log('GAME_JOIN')
    logger.log(data)

    try {
      logger.log('connectToLobby')
      await client.connectToLobby('test', data.secret)
      logger.log('connectToLobby ok')
    } catch (err) {
      logger.log('connectToLobby Error!')
      logger.log(err)
    }
  })
  client.subscribe('GAME_SPECTATE', data => {
    logger.log('GAME_SPECTATE')
    logger.log(data)
  })

  client.subscribe('ACTIVITY_JOIN', data => {
    logger.log('ACTIVITY_JOIN')
    logger.log(data)
  })
  client.subscribe('ACTIVITY_JOIN_REQUEST', data => {
    logger.log('ACTIVITY_JOIN_REQUEST')
    logger.log(data)
  })
  client.subscribe('ACTIVITY_SPECTATE', data => {
    logger.log('ACTIVITY_SPECTATE')
    logger.log(data)
  })

  client.subscribe('LOBBY_DELETE', data => {
    logger.log('LOBBY_DELETE')
    logger.log(data)
  })
  client.subscribe('LOBBY_UPDATE', data => {
    logger.log('LOBBY_UPDATE')
    logger.log(data)
  })
  client.subscribe('LOBBY_MEMBER_CONNECT', data => {
    logger.log('LOBBY_MEMBER_CONNECT')
    logger.log(data)
  })
  client.subscribe('LOBBY_MEMBER_DISCONNECT', data => {
    logger.log('LOBBY_MEMBER_DISCONNECT')
    logger.log(data)
  })
  client.subscribe('LOBBY_MEMBER_UPDATE', data => {
    logger.log('LOBBY_MEMBER_UPDATE')
    logger.log(data)
  })
  client.subscribe('LOBBY_MESSAGE', data => {
    logger.log('LOBBY_MESSAGE')
    logger.log(data)
  })
}

exports.updateState = async state => {
  activity.state = state
  await client.setActivity(activity)
}

exports.updateDetails = async details => {
  activity.details = details
  await client.setActivity(activity)
}

exports.shutdownRPC = async () => {
  if (!client) return
  await client.clearActivity()
  await client.destroy()
  client = null
  activity = null
}